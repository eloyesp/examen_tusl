HTML
----
    
1. ¿Cuál es la utilidad del atributo "alt" en las imágenes y por qué es
   importante?

2. ¿Cuáles son los elementos y los atributos fundamentales en un formulario
   web?

CSS
---
    
1. ¿Cuál es el selector que se aplica sobre todos los elementos p dentro de un
   elemento div?

    `div p`

    `div.p`

    `div + p`

    `div > p`

    `p`

    `div, p`

2. Explique qué significan la siguientes reglas de css

    `background: #000 url(images/bg.gif) no-repeat top right; margin: 1em 2em 0;
    padding: 1em 2em; border-size: 2px;`


JavaScript
----------

1. ¿Cuál es la sintaxis correcta para cambiar todos los párrafos en un documento
   HTML?

    `document.getElementByName("p").innerHTML = "Hola mundo!"`
    
    `document("p").innerHTML = "Hola mundo!"`
    
    `p.setInnerHTML("Hola mundo!")`
    
    `document.querySelectorAll("p").forEach(function(para) {para.innerHTML = 'Hola' })`

2. ¿Cuál es la funcion de javascript en un documento HTML?

3. ¿Javascript y Java son el mismo lenguaje?

4. ¿Por qué es tan popular JavaScript, es el mejor lenguaje de programación?
